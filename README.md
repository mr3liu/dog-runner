# Dog Runner

Modification of Google Chrome’s open-source *T-Rex Runner* browser game with additional features and retexturing.

Play at <https://student.cs.uwaterloo.ca/~mr3liu/dog_runner/>

The license governing the original source code by The Chromium Authors can be found in the LICENSE file.
